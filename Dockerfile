FROM smebberson/alpine-base:3.1.0

MAINTAINER andrew@shrewhome.net

LABEL description="Base image built from smebberson/alpine-base with up-to-date confd installed"

# Confd environment
ENV CONFD_BACKEND=env \
    CONFD_MAX_FAILCOUNT=5 \
    CONFD_OPTS= \
    CONFD_UPDATE_INTERVAL=30 \
    CONFD_VCS_URL=github.com/kelseyhightower/confd \
    CONFD_VERSION=0.12.0-alpha3

# Download and compile confd
RUN apk add --update --no-cache --virtual .confd-dependencies go git gcc musl-dev && \
    mkdir -p /src/go/src/$CONFD_VCS_URL && \
    git clone --quiet --branch "v$CONFD_VERSION" https://$CONFD_VCS_URL.git /src/go/src/$CONFD_VCS_URL && \
    cd /src/go/src/$CONFD_VCS_URL && \
    GOPATH=/src/go go build -a -installsuffix cgo -ldflags '-extld ld -extldflags -static' -x . && \
    mv ./confd /bin/ && \
    chmod +x /bin/confd && \
    apk del .confd-dependencies && \
    rm -rf /src

# Copy root fs skeleton
COPY root /

ARG BUILD_DATE
ARG VCS_REF
ARG VERSION

LABEL version="$VERSION" \
      org.label-schema.build-date="$BUILD_DATE" \
      org.label-schema.vcs-ref="$VCS_REF" \
      org.label-schema.vcs-url="https://gitlab.com/andrewheberle/docker-alpine-base" \
      org.label-schema.version="$VERSION" \
      org.label-schema.schema-version="1.0"